using System;
using System.Collections.Generic;
using System.Linq;
using LessonInterface;
using LessonInterfaceUnit3;
using LessonInterfaceUnit3.AccountService;
using LessonInterfaceUnit3.Data;
using Moq;
using NUnit.Framework;

namespace InterfacesTest
{
    [TestFixture]
    public class Tests
    {
        Mock<IRepository<Account>> mock = new Mock<IRepository<Account>>();
        Account account1 = new Account("first", "lastname", new DateTime(2016, 2, 29));
        Account account2 = new Account("first", "lastname2", new DateTime(2016, 2, 29));
        
        [SetUp]
        public void Setup()
        {
            
        }

        [Test]
        public void TestAddAccount()
        {
            mock
                .Setup(rp => rp.Add(It.IsAny<Account>())).Verifiable();

            IAccountService accountService = new AccountService(mock.Object);
            accountService.AddAccount(account1);
            accountService.AddAccount(account1);

            mock.Verify(rep => rep.Add(It.IsAny<Account>()));
            
        }
        
        [Test]
        public void AddAccountServiceTest()
        {

            var repository = new Mock<Repository>();

            AccountService accountService = new AccountService(repository.Object);
            accountService.AddAccount(account1);
            var ex = Assert.Throws<ArgumentException>(() => accountService.AddAccount(account2));

            Assert.AreEqual($"This name \"{account2.FirstName}\" have in database", ex.Message);
        }


        [Test]
        public void AddAccountServiceExceptionTest()
        {
            
            const string errorMessage = "This name have in database";
            
            var accountService = new AccountService(mock.Object);
            Account account3 = new Account("", "lastname", new DateTime(2016, 2, 29));
            //accountService.AddAccount(account3);

            Assert.Throws<ArgumentException>(() => accountService.AddAccount(account3));
            Assert.Throws<MockException>(() => mock.Verify(repo => repo.Add(It.IsAny<Account>())));

            
            //Assert.Throws<ArgumentException>(() => accountService.AddAccount(account2));

        }


        [Test]
        public void AddAccountTest()
        {
            var accountService = new Mock<IAccountService>();
            Repository repository = new Repository(accountService.Object);
            repository.Add(account1);

            Assert.That(repository.Accounts.Count, Is.EqualTo(1));
        }

        [Test]
        [TestCase("name", "name")]
        [TestCase("name1", "name1")]
        public void AddAccountExceptionTest(string firstname, string secondname)
        {
            var mock = new Mock<IRepository<Account>>();

            var accountService = new AccountService(mock.Object);
            accountService.AddAccount(account1);
            accountService.AddAccount(account2);

            mock.Verify(x => x.Add(It.IsAny<Account>()));
            mock.Verify(x => x.Add(It.IsAny<Account>()), Times.AtLeast(2));
        }

        [Test]
        public void AccountServiceTest_Validation_Age()
        {
            var newUser = new Account("Test", "lastname", DateTime.Now.AddYears(-10));
            var mock = new Mock<IRepository<Account>>();
            
            
            var accountService = new AccountService(mock.Object);
            accountService.AddAccount(newUser);
            var result = accountService.CheckAge();

            Assert.That(result,Is.LessThanOrEqualTo(18));
        }


        [Test]
        public void NameRequired()
        {
            var mock = new Mock<IAccountService>();


            var result = mock.Object.CheckName();

            Assert.That(result, Is.EqualTo(false));
        }

        [Test]
        public void AccountNameRequired()
        {
            IAccountService accountService = Mock.Of<IAccountService>(d => d.CheckName());
            var name = accountService.CheckLastName();
            Assert.That(name, Is.EqualTo(false));
        }

        [Test]
        public void AccountLastNameRequired()
        {
            IAccountService accountService = Mock.Of<IAccountService>(d => d.CheckLastName());
            var name = accountService.CheckLastName();
            Assert.That(name, Is.EqualTo(true));
        }

        [Test]
        public void AccountCheckAge()
        {
            IAccountService accountService = Mock.Of<IAccountService>(d => d.CheckAge() == 18);
            var name = accountService.CheckAge();
            Assert.That(name, Is.EqualTo(18));
        }
    }
}