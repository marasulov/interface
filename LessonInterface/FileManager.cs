using System;
using System.IO;

namespace LessonInterface
{
    public class FileManager
    {
        public void Save(string path, string valueToWrite)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write))
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    sw.WriteLine(valueToWrite);
                }

                Console.WriteLine("Запись выполнена");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void Read(string path, ISerializer serializer)
        {
            try
            {
                FileStream stream = new FileStream(path,FileMode.Open);
                
                OtusStreamReader<Person> streamReader = new OtusStreamReader<Person>(stream, serializer);

                foreach (var p in streamReader)
                {
                    Console.WriteLine(p.Name);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
    }
}