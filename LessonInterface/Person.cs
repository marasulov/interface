﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LessonInterface
{
    public class Person
    {
        public int IdNumber { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public DateTime RegistrationDate { get; set; }

        public Person(int number, string name, string lastName, DateTime registrationDate)
        {
            IdNumber = number;
            Name = name;
            LastName = lastName;
            RegistrationDate = registrationDate;
        }

        public Person()
        {
        }

    }
}