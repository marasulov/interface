﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using Newtonsoft.Json;

namespace LessonInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person1 = new Person(1, "Name1", "Lastname2", DateTime.Now);
            Person person2 = new Person(2, "Name3", "Lastname3", DateTime.Now);
            Person person3 = new Person(3, "Name2", "Lastname1", DateTime.Now);

            List<Person> persons = new List<Person>();
            persons.Add(person1);
            persons.Add(person2);
            persons.Add(person3);

            ISerializer serializer = new OtusJsonSerializer();
            SerializeManager serializeManager = new SerializeManager(serializer);

            string doc = serializeManager.Serialize(persons);

            FileManager fileManager = new FileManager();
            fileManager.Save(serializeManager.filename, doc);

            //чтение
            fileManager.Read(serializeManager.filename, serializer);
        }
    }
}