﻿using System.IO;

namespace LessonInterface
{
    public interface ISerializer
    {
        string Serialize<T>(T item);
        T Deserialize<T>(Stream stream);
        string filename { get; }
    }
}