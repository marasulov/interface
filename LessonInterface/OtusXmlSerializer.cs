﻿using System;
using System.IO;
using System.Xml;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;

namespace LessonInterface
{
    public class OtusXmlSerializer:ISerializer
    {
         
        private string _filename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "//1.xml";
        private IExtendedXmlSerializer serializer = new ConfigurationContainer().UseOptimizedNamespaces().Create();

        public string filename => _filename;

        public OtusXmlSerializer()
        {
        }

        public T Deserialize<T>(Stream stream)
        {
            IExtendedXmlSerializer serializer = new ConfigurationContainer().Create();            
            T instance = serializer.Deserialize<T>(new XmlReaderSettings{IgnoreWhitespace = false}, stream);

            return instance;
        }

        public string Serialize<T>(T item)
        {
            var document = serializer.Serialize(new XmlWriterSettings {Indent = true}, item);
            return document;
        }
    }
}