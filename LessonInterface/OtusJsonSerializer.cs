﻿using System;
using System.IO;
using Newtonsoft.Json;


namespace LessonInterface
{
    public class OtusJsonSerializer:ISerializer
    {
        
        private string _filename = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "//1.json";

        public string filename => _filename;

        public OtusJsonSerializer()
        {

        }

        public T Deserialize<T>(Stream stream)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (var sr = new StreamReader(stream))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return (T)serializer.Deserialize(jsonTextReader, typeof(T));
            }
        }

        public string Serialize<T>(T item)
        {
            return JsonConvert.SerializeObject(item, Formatting.Indented);
        }
    }
}