﻿using System.IO;

namespace LessonInterface
{
    public class SerializeManager
    {
        private readonly ISerializer _serializer;

        public string filename => _serializer.filename;
        
        public SerializeManager(ISerializer serializer)
        {
            _serializer = serializer;
            
        }

        public string Serialize<T>(T item)
        {
            return _serializer.Serialize(item);
        }
        public T Deserialize<T>(Stream stream)
        {
            return _serializer.Deserialize<T>(stream);
        }
    }
}