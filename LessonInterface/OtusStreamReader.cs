﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace LessonInterface
{
    public class OtusStreamReader<T>: IEnumerable<T>, IDisposable 
    {
        private readonly Stream _stream;
        private readonly ISerializer _serializer;
        private List<T> obj;

        public OtusStreamReader(Stream stream, ISerializer serializer)
        {
            _stream = stream;
            _serializer = serializer;
            obj =_serializer.Deserialize<List<T>>(_stream);
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < obj.Count; i++)
            {
                yield return obj[i];                
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Dispose()
        {
            _stream.Dispose();
        }
    }
}