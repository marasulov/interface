﻿using LessonInterface;
using System;
using System.Collections.Generic;

namespace LessonInterfaceUnit2
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person(8, "Name1", "Lastname2", DateTime.Now);
            Person person1 = new Person(6, "Name3", "Lastname3", DateTime.Now);
            Person person2 = new Person(3, "Name2", "Lastname1", DateTime.Now);

            List<Person> persons = new List<Person>();
            persons.Add(person);
            persons.Add(person1);
            persons.Add(person2);

            PersonSorter personSorter = new PersonSorter();
            IEnumerable<Person> people = personSorter.Sorter(persons);

            foreach (var item in people)
            {
                Console.WriteLine($"{item.IdNumber} - {item.Name}");
            }
        }
    }
}