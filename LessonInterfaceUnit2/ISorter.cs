﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LessonInterfaceUnit2
{
    public interface ISorter<T>
    {
        IEnumerable<T> Sorter(IEnumerable<T> notSortedItems);
    }
}
