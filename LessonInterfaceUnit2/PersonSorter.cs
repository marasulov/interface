﻿using LessonInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LessonInterfaceUnit2
{
    class PersonSorter:ISorter<Person>
    {
        public IEnumerable<Person> Sorter(IEnumerable<Person> notSortedItems)
        {
            var sortedPeople = (IEnumerable<Person>)notSortedItems.OrderBy(x => x.IdNumber);
            return sortedPeople;
        }
    }
}
