﻿using System.Collections.Generic;

namespace LessonInterfaceUnit3.AccountService
{
    public interface IAccountService
    {
        // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
        // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
        void AddAccount(Account account);
        bool CheckName();
        bool CheckLastName();
        int CheckAge();
    }
}
