using System;
using System.Collections.Generic;
using System.Linq;
using LessonInterfaceUnit3.Data;

namespace LessonInterfaceUnit3.AccountService
{
    public class AccountService:IAccountService
    {
        private Account _account;
        private IRepository<Account> _repository;

        public AccountService(IRepository<Account> repository)
        {
            _repository = repository;
        }

        public void AddAccount(Account account)
        {
            _account = account;
            if(!CheckName() & !CheckLastName() & CheckAge()<18) return;
            _repository.Add(_account);
        }
        
        public bool CheckName()
        {
            if (string.IsNullOrEmpty(_account.FirstName)) throw new ArgumentException("Name have not be empty");
            return true;
        }
        public bool CheckLastName()
        {
            if (string.IsNullOrEmpty(_account.LastName)) return false;
            return true;
        }
        public int CheckAge()
        {
            var today = DateTime.Today;
            var age = today.Year - _account.BirthDate.Year;
            return age;
        }

    }
}