﻿using System;
using System.Collections;
using System.Collections.Generic;
using LessonInterface;
using LessonInterfaceUnit3.Data;

namespace LessonInterfaceUnit3
{
    class Program
    {
        static void Main(string[] args)
        {
            // В классе реализаторе сохранять данные в какой-нибудь файл. Формат на ваше усмотрение - json, xml, csv, 
            // В классе-реализаторе делать валидацию: проверить что имена не пустые, что возраст > 18 лет, можете также добавить свои правила
            // Если валидация прошла успешно, то добавлять аккаунт в репозиторий
            //TODO

            //список аккаунтов
            Account account1 = new Account("first", "lastname", new DateTime(2016, 2, 29));
            Account account2 = new Account("name", "lastname", new DateTime(2016, 2, 29));
            Account account3 = new Account("first", "lastname", new DateTime(2016, 2, 29));
            List<Account> accounts = new List<Account>();
            accounts.Add(account1);
            accounts.Add(account2);
            accounts.Add(account3);

            //сериализация
            ISerializer serializer = new OtusJsonSerializer();
            SerializeManager serializeManager = new SerializeManager(serializer);

            Repository repository = new Repository();
            foreach (var account in accounts)
            {
                repository.Add(account);
            }
            FileManager fileManager = new FileManager();
            string path = serializer.filename;
            string serializedJson = serializer.Serialize(repository.GetAll());
            fileManager.Save(path, serializedJson);

            var accountsRepository = repository.GetAll().GetEnumerator();
            while (accountsRepository.MoveNext())
            {
                Console.WriteLine(accountsRepository.Current.FirstName);
            }

            Console.Read();
        }
    }
}