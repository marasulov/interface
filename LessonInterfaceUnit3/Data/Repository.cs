using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using LessonInterface;
using LessonInterfaceUnit3.AccountService;

namespace LessonInterfaceUnit3.Data
{
    public class Repository : IRepository<Account>
    {
        private List<Account> _accounts = new List<Account>();

        public List<Account> Accounts
        {
            get => _accounts;
            set => _accounts = value;
        }

        private ISerializer _serializer;
        private IAccountService _accountService;

        
        

        public Repository(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public int Count { get; set; }

        public Repository()
        {
        }


        public IEnumerable<Account> GetAll()
        {
            for (int i = 0; i < Count; i++)
            {
                yield return _accounts[i];
            }
        }
        
        public Account GetOne(Func<Account, bool> predicate)
        {
            Account account = _accounts.FirstOrDefault(predicate);
            return account;
        }

        public void Add(Account item)
        {
            if(!CheckUniqName(item.FirstName))
            {
                throw new ArgumentException($"This name \"{item.FirstName}\" have in database");
            }
            
            _accounts.Add(item);
            Count = _accounts.Count;
            
        }
        private bool CheckUniqName(string name)
        {
            Account account = GetOne(x => x.FirstName == name);
            return account == null;
        }
        
    }
}