﻿using System;
using System.Collections.Generic;

namespace LessonInterfaceUnit3.Data
{
    public interface IRepository<T>
    {
        // когда реализуете этот метод, используйте yield (его можно использовать просто в методе, без создания отдельного класса)
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> predicate);
        void Add(T item);
        int Count { get; set; }
    }
}
